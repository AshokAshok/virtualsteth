//
//  AppointmentCollectionViewCell.swift
//  VirtualSteth
//
//  Created by suja on 26/06/21.
//

import UIKit

class AppointmentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var rescheduleBtn: UIButton!
    @IBOutlet weak var invoiceBtn: UIButton!
    @IBOutlet weak var callStatusLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var specializationLbl2: UILabel!
    @IBOutlet weak var specializationLbl1: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var doctorNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
