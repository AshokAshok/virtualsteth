//
//  SpecializationsCollectionViewCell.swift
//  VirtualSteth
//
//  Created by suja on 27/06/21.
//

import UIKit

class SpecializationsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var specializationProfilePic: UIImageView!
    @IBOutlet weak var specializationsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
