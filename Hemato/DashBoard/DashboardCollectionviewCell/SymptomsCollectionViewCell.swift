//
//  SymptomsCollectionViewCell.swift
//  VirtualSteth
//
//  Created by suja on 26/06/21.
//

import UIKit

class SymptomsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var symptomsProfilePic: UIImageView!
    @IBOutlet weak var symptomsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
