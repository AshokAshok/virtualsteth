//
//  ProductsTableViewCell.swift
//  Hemato
//
//  Created by ashok quad on 16/05/21.
//

import UIKit

class ProductsTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var offerButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceLabel1: UILabel!
    
    static var identifier: String {
           return String(describing: self)
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
