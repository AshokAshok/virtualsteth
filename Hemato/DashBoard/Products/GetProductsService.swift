//
//  GetProductsService.swift
//  Hemato
//
//  Created by RamaKrishna on 20/05/21.
//

import Foundation

struct ProductRequest: Codable {
    let categoryId: String
    
    enum CodingKeys: String, CodingKey {
        case categoryId = "category_id"
    }
}

struct Products: Codable {
    let productId: String
    let name: String
    let category: String
    let subCategory: String
    let price: String
    let discountPrice: String
    let iconUrl: String
    let discount: Int
    let status: Int
    
    enum CodingKeys: String, CodingKey {
        case productId = "product_id"
        case name = "product_name"
        case category = "product_category"
        case subCategory = "product_subcategory"
        case price = "product_price"
        case discountPrice = "discount_price"
        case iconUrl = "pic"
        case discount = "discount"
        case status = "success"
    }
}

protocol GetProductsService {
    func getProducts(request: ProductRequest, onCompletion: @escaping (Result<[Products], Error>) -> Void)
}

struct GetProductsServiceImp: GetProductsService {
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func getProducts(request: ProductRequest, onCompletion: @escaping (Result<[Products], Error>) -> Void) {
        let networkRequest = createNetworkRequest(request: request)
        
        networkClient.sendRequest(request: networkRequest) { (data, response, error) in
            if let data = data,
               let products =  try? JSONDecoder().decode([Products].self, from: data) {
                onCompletion(.success(products))
            } else {
                onCompletion(.failure(NetworkError.unknown()))
            }
        }
    }
    
    func createNetworkRequest(request: ProductRequest) -> NetworkRequest {
        let subURL = "categorybasedproducts?category_id=\(request.categoryId)"
        let endpointURL = baseURL + subURL
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .get,
                                            data: nil)
        return networkRequest
    }
}
