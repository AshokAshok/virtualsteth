//
//  ForgotPasswordViewController.swift
//  Hemato
//
//  Created by ashok quad on 15/05/21.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceLabel1: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var prescriptionDesLabel: UILabel!
    @IBOutlet weak var instructionsDesLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func qtyButtonClicked(_ sender: UIButton) {
        if let text=qtyLabel.text
        {
            var qty: Int=Int(text)!
            
            if sender.tag==1 {
                if qty>1 {
                    qty=qty-1
                }
            } else {
                qty=qty+1
            }
            
            qtyLabel.text="\(qty)"
        }

    }
    
    func increaseQuanityButtonAction() {
        
    }
    
    func decreaseQuantityButtonAction() {
        
    }
    
    @IBAction func addButtonClicked(_ sender: UIButton) {
    }
    @IBAction func cartButtonClicked(_ sender: UIBarButtonItem) {
    }
    
}
