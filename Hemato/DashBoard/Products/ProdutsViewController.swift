//
//  ForgotPasswordViewController.swift
//  Hemato
//
//  Created by ashok quad on 15/05/21.
//

import UIKit

class ProdutsViewController: UIViewController {
    @IBOutlet weak var productsTableView: UITableView!
    
    var productsArray: [Products] = []
    
    var produtsViewModel: ProductsViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        productsTableView.register(UINib(nibName: ProductsTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ProductsTableViewCell.identifier)
        
        //produtsViewModel = ProductsViewModel()
        produtsViewModel.delegate=self
        
        self.view.showActivityIndicator()
        produtsViewModel.getProducts(request: ProductRequest(categoryId: produtsViewModel.categories.id))
    }

}

extension ProdutsViewController: ProductsViewDelegate {
    func onProductsSuccess(response: [Products]) {
        self.view.hideActivityIndicator()
        productsArray=response
        productsTableView.reloadData()
    }
    
    func onProductsFailure(message: String) {
        self.view.hideActivityIndicator()
        UIAlertController.show(title: "", message: message, on: self)
    }
    
    
}

extension ProdutsViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductsTableViewCell.identifier, for: indexPath) as? ProductsTableViewCell else {
            return UITableViewCell()
        }
        
        cell.nameLabel.text = productsArray[indexPath.row].name
        cell.productImageView.imageFromServerURL(urlString: productsArray[indexPath.row].iconUrl, PlaceHolderImage: UIImage(named: "logo")!)
        cell.priceLabel.text = productsArray[indexPath.row].discountPrice
        cell.priceLabel1.text = productsArray[indexPath.row].price
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard: UIStoryboard=UIStoryboard.init(name: "Product", bundle: nil)
        
        let viewController: ProductDetailsViewController=storyBoard.instantiateViewController(identifier: "ProdutsDetailViewController") as! ProductDetailsViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}
