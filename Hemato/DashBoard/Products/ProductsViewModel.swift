//
//  ProductsViewModel.swift
//  Hemato
//
//  Created by RamaKrishna on 20/05/21.
//

import Foundation

protocol ProductsViewDelegate {
    func onProductsSuccess(response: [Products])
    func onProductsFailure(message: String)
}

class ProductsViewModel {
    let productsService: GetProductsService
    var delegate: ProductsViewDelegate?
    
    var categories: Category
    
    init(categorieResponse: Category, service: GetProductsService = GetProductsServiceImp()) {
        self.productsService = service
        
        self.categories=categorieResponse
    }
    
    func getProducts(request: ProductRequest) {
        self.productsService.getProducts(request: request) { (result: Result<[Products], Error>) in
            switch result {
            case .success(let products):
                self.delegate?.onProductsSuccess(response: products)
            case .failure(let error):
                let errorMessage = NetworkError.errorMsg(error as! NetworkError)
                
                self.delegate?.onProductsFailure(message: errorMessage())
            }
        }
    }
}
