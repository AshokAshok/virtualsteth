//
//  GetCategoriesService.swift
//  Hemato
//
//  Created by RamaKrishna on 20/05/21.
//

import Foundation

struct Category: Codable {
    let id: String
    let name: String
    let iconURL: String
    
    enum CodingKeys: String, CodingKey {
        case id = "category_id"
        case name = "category_name"
        case iconURL = "pic"
    }
    
}

protocol GetCategoriesService {
    func getCategories(onCompletion: @escaping (Result<[Category], Error>) -> Void)
}

struct GetCategoriesServiceImpl: GetCategoriesService {
    
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func getCategories(onCompletion: @escaping (Result<[Category], Error>) -> Void) {
        let networkRequest = createNetworkRequest()
        networkClient.sendRequest(request: networkRequest) { (data, urlResponse, error) in
            if let data = data,
               let categories =  try? JSONDecoder().decode([Category].self, from: data) {
                onCompletion(.success(categories))
            } else {
                onCompletion(.failure(NetworkError.unknown()))
            }
        }
    }
    
    func createNetworkRequest() -> NetworkRequest {
        let subURL = "categories"
        let endpointURL = baseURL + subURL
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .get,
                                            data: nil)
        return networkRequest
    }
}
