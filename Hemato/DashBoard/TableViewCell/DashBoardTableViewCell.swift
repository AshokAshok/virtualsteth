//
//  DashBoardTableViewCell.swift
//  Hemato
//
//  Created by ashok quad on 16/05/21.
//

import UIKit

class DashBoardTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    static var identifier: String {
           return String(describing: self)
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
