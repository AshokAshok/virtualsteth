//
//  AppointmentViewAllViewController.swift
//  VirtualSteth
//
//  Created by suja on 27/06/21.
//

import UIKit

class AppointmentViewAllViewController: UIViewController {

    @IBOutlet weak var appointmentCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    appointmentCollectionView.register(UINib(nibName: "AppointmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AppointmentCollectionViewCell")

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AppointmentViewAllViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
            return 1
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 10
       
        }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "AppointmentCollectionViewCell", for: indexPath) as! AppointmentCollectionViewCell
            
            return cell
      
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
            appointmentCollectionView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
            return CGSize(width: 310, height: 250)
   }
}
