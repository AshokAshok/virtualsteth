//
//  SymptomsViewAllViewController.swift
//  VirtualSteth
//
//  Created by suja on 27/06/21.
//

import UIKit

class SymptomsViewAllViewController: UIViewController {

    @IBOutlet weak var symptomsCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title="Symptoms"
        symptomsCollectionView.register(UINib(nibName: "SymptomsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SymptomsCollectionViewCell")
        // Do any additional setup after loading the view.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SymptomsViewAllViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
            return 1
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 10
       
        }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "SymptomsCollectionViewCell", for: indexPath) as! SymptomsCollectionViewCell
            
            return cell
      
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let noOfCellsInRow = 3

        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

       let size = Int((symptomsCollectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

       return CGSize(width: size, height: size+20)
   }
}
