//
//  SpecializationViewAllViewController.swift
//  VirtualSteth
//
//  Created by suja on 27/06/21.
//

import UIKit

class SpecializationViewAllViewController: UIViewController {

    @IBOutlet weak var specializationCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title="Specializations"
        
        specializationCollectionView.register(UINib(nibName: "SpecializationsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SpecializationsCollectionViewCell")
        
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SpecializationViewAllViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
            return 1
        
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return 10
       
        }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell=collectionView.dequeueReusableCell(withReuseIdentifier: "SpecializationsCollectionViewCell", for: indexPath) as! SpecializationsCollectionViewCell
            
            return cell
      
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

                let numberofItem: CGFloat = 3

                let collectionViewWidth = specializationCollectionView.bounds.width

                let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing

                let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left

                let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)

                print(width)

                return CGSize(width: width, height: width)
   }
}
