//
//  DashBoardViewModel.swift
//  Hemato
//
//  Created by RamaKrishna on 20/05/21.
//

import Foundation

protocol DashboardViewDelegate {
    func onCategoriesSuccess(response: [Category])
    func onCategoriesFailure(message: String)
}

class DashBoardViewModel {
    
    let service: GetCategoriesService
    var delegate: DashboardViewDelegate?
    
    init(service: GetCategoriesService = GetCategoriesServiceImpl()) {
        self.service = service
    }
    
    func getCategories() {
        service.getCategories { (result: Result<[Category], Error>) in
            switch result {
            case .success(let categories):
                self.delegate?.onCategoriesSuccess(response: categories)
            case .failure(let error):
                let errorMessage = NetworkError.errorMsg(error as! NetworkError)
                
                self.delegate?.onCategoriesFailure(message: errorMessage())
            }
        }
        
    }
    
    
}
