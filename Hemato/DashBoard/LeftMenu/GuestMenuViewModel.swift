//
//  LeftMenuGuestViewModel.swift
//  Hemato
//
//  Created by RamaKrishna on 22/05/21.
//

import Foundation

class GuestMenuViewModel: MenuViewModel {
    
    private let menuItems: [MenuItem] = [.dashboard, .myOrders, .myCoupons, .profileSettings, .changePassword, .about, .contactUs, .logout]
    
    init() {

    }
    
    var userName: String {
      return "Vikas"
    }
    
    var email: String {
        return "+91 9989690490"
    }
    
    func getNumberOfItems() -> Int {
        return menuItems.count
    }
    
    func getItemAt(index: Int) -> MenuItem {
        return menuItems[index]
    }
    
    
}
