//
//  LeftMenuViewModel.swift
//  Hemato
//
//  Created by RamaKrishna on 22/05/21.
//

import Foundation

protocol MenuViewModel {
    var userName: String { get }
    var email: String { get }
    func getNumberOfItems() -> Int
    func getItemAt(index: Int) -> MenuItem
}

class LoggedInMenuViewModel: MenuViewModel {
    
    private let menuItems: [MenuItem] = [.dashboard, .myOrders, .myCoupons, .profileSettings, .changePassword, .about, .contactUs, .logout]
    
    private let user: User
    
    init(user: User) {
        self.user = user
    }
    
    var userName: String {
        return user.fullName ?? ""
    }
    
    var email: String {
        return user.email ?? ""
    }
    
    func getNumberOfItems() -> Int {
        return menuItems.count
    }
    
    func getItemAt(index: Int) -> MenuItem {
        return menuItems[index]
    }
    
}
