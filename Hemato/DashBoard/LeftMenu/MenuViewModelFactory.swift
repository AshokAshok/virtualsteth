//
//  LeftMenuViewModelFactory.swift
//  Hemato
//
//  Created by RamaKrishna on 22/05/21.
//

import Foundation

struct MenuViewModelFactory {
    
    static func createViewModel(role: HTUserRole) -> MenuViewModel {
        switch role {
        case .loggedIn:
            let user = HTUserDefaults.shared.getUser()
            
            return LoggedInMenuViewModel(user: user!)
        case .guest:
            return GuestMenuViewModel()
        
        }
    }
}
