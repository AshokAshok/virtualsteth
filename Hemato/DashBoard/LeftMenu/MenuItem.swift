//
//  MenuItem.swift
//  Hemato
//
//  Created by RamaKrishna on 22/05/21.
//

import Foundation

enum MenuItem: String {
    case dashboard = "My Appointments"
    case myOrders = "My Prescriptions"
    case myCoupons = "Change Password"
    case profileSettings = "Logged in Devices"
    case changePassword = "My Wallet"
    case about = "Call Us"
    case contactUs = "Help & Support"
    case login = "Login"
    case logout = "Logout"
    
    var name: String {
        return self.rawValue
    }
}
