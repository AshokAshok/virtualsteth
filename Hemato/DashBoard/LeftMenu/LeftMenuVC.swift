//
//  LeftMenuVC.swift
//  FAPanels
//
//  Created by Fahid Attique on 17/06/2017.
//  Copyright © 2017 Fahid Attique. All rights reserved.
//

import UIKit

class LeftMenuVC: UIViewController {
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    var viewModel: MenuViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel = MenuViewModelFactory.createViewModel(role: HTUser.shared.currentRole)
        updateUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupUI() {
        tableView.register(UINib.init(nibName: "LeftMenuCell", bundle: nil), forCellReuseIdentifier: "LeftMenuCell")
    }
    
    
    func updateUI()
    {
        nameLabel.text = viewModel.userName
        emailLabel.text = viewModel.email
        tableView.reloadData()
    }
}

extension LeftMenuVC: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell") as! LeftMenuCell
        
        let item = viewModel.getItemAt(index: indexPath.row)
        cell.menuOption.text = item.name
        //cell.menuImage.image = UIImage(named: "right_menu_" + String(indexPath.row + 1))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let item = viewModel.getItemAt(index: indexPath.row)
        switch item {
        case .dashboard:
            dashboardAction()
        case .myOrders:
            myOrdersAction()
        case .myCoupons:
            myCouponsAction()
        case .profileSettings:
            profileSettingsAction()
        case .changePassword:
            changePasswordAction()
        case .about:
            aboutAction()
        case .contactUs:
            contactUsAction()
        case .login:
            loginAction()
        case .logout:
            logoutAction()
        }
    }
    
}

//MARK: Dashboard
extension LeftMenuVC {
    func dashboardAction() {
        self.selectedViewController(storyBoard: "Product",
                                    identifier: "DashBoardViewController")
    }
    
    func selectedViewController(storyBoard: String,
                                identifier: String) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: storyBoard, bundle: nil)
        
        let centerVC = mainStoryboard.instantiateViewController(withIdentifier: identifier)
        let centerNavVC = UINavigationController(rootViewController: centerVC)
        
        centerNavVC.navigationBar.barTintColor = UIColor(named: "PrimaryColor")
        
        panel!.configs.bounceOnCenterPanelChange = true
        
        panel!.center(centerNavVC, afterThat: {
            print("Executing block after changing center panelVC From Left Menu")
            //_ = self.panel!.left(nil)
        })
    }
}

//MARK: My Orders
extension LeftMenuVC {
    
    func myOrdersAction() {
        
    }
}

//MARK: My Coupons
extension LeftMenuVC {
    
    func myCouponsAction() {
        
    }
}

//MARK: Profile Settings
extension LeftMenuVC {
    
    func profileSettingsAction() {
        
    }
}

//MARK: Change Password
extension LeftMenuVC {
    
    func changePasswordAction() {
        
    }
    
}

//MARK: About
extension LeftMenuVC {
    
    func aboutAction() {
        
    }
    
}

//MARK: ContactUs
extension LeftMenuVC {
    
    func contactUsAction() {
        
    }
    
}

//MARK: LogIn
extension LeftMenuVC {
    
    func loginAction()  {
        self.navigateToLoginVC()
    }
    
}
//MARK: Logout
extension LeftMenuVC {
    func logoutAction(){
        let alert = UIAlertController(title: "Logout?", message: "Do you want to logout?",         preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "Logout",
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        HTUserDefaults.shared.deleteUser()
                                        self.navigateToLoginVC()
                                      }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func navigateToLoginVC() {
        let storyBoard: UIStoryboard = UIStoryboard.init(name: "Authentication", bundle: nil)
        let logInNavigationVC: UINavigationController = storyBoard.instantiateViewController(identifier: "LoginNavigationVC") as! UINavigationController
        UIApplication.shared.windows.first?.rootViewController = logInNavigationVC
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}
