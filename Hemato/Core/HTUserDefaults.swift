//
//  HTUserDefaults.swift
//  Hemato
//
//  Created by RamaKrishna on 20/05/21.
//

import Foundation

struct HTUserDefaults {
    
    static let shared = HTUserDefaults()
    
    private init() {
        
    }
    
    func getUser() -> User? {
        if let userData = UserDefaults.standard.object(forKey: "User") as? Data,
           let user = try? JSONDecoder().decode(User.self, from: userData) {
            return user
        } else {
            return nil
        }
    }
    
    func save(user: User) {
        if let encodedUser = try? JSONEncoder().encode(user) {
            UserDefaults.standard.set(encodedUser, forKey: "User")
        }
    }
    
    func deleteUser()  {
        UserDefaults.standard.removeObject(forKey: "User")
    }
}
