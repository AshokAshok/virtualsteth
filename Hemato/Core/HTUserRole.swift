//
//  HTUserRole.swift
//  Hemato
//
//  Created by RamaKrishna on 22/05/21.
//

import Foundation

enum HTUserRole {
    case guest
    case loggedIn
}
