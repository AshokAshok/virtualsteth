//
//  HTUser.swift
//  Hemato
//
//  Created by RamaKrishna on 22/05/21.
//

import Foundation

struct HTUser {
    static let shared = HTUser()
    
    private init() {
        
    }
    
    var currentRole: HTUserRole {
        if let _ = HTUserDefaults.shared.getUser() {
            return .loggedIn
        }
        else {
            return .guest
        }
    }
    
}
