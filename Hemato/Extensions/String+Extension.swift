//
//  String+Extension.swift
//  Hemato
//
//  Created by RamaKrishna on 19/05/21.
//

import Foundation

extension String {
    
    /// This method encodes the string
    ///
    /// - Returns: returns encoded string
    func encodededString() -> String {
        let encodedStr = self.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]{} ").inverted)
        return encodedStr ?? self
    }
}
