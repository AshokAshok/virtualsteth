//
//  Dictionary+Extension.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

extension Dictionary {
    
    func getJsonData() -> Data? {
        var jsonData:Data? = nil
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        }
        catch {
            print("Error -> \(error)")
        }
        return jsonData
    }
}
