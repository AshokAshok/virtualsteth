//
//  UIAlertController+Extension.swift
//  Hemato
//
//  Created by RamaKrishna on 19/05/21.
//

import Foundation
import UIKit
extension UIAlertController {
    
    static func show(title: String?,
                   message: String?,
                   on viewController: UIViewController,
                   onCompletion: (() -> ())? = nil) {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { action in
            onCompletion?()
        })
        
        alertVC.addAction(okAction)
        viewController.present(alertVC, animated: true)
    }
}
