//
//  ViewInspectable.swift
//  Hemato
//
//  Created by ashok quad on 15/05/21.
//

import Foundation

import UIKit

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    
    /// This method shows activity indicator on view
    func showActivityIndicator()
    {
        let view = UIView(frame: self.bounds)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.tag = 99998888
        //Activity Indicator
        let activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
        activityIndicator.color = .black
        activityIndicator.tag = 007
        activityIndicator.center = CGPoint(x: self.bounds.width/2, y:self.bounds.height/2)
        view.addSubview(activityIndicator)
        self.addSubview(view)
        self.bringSubviewToFront(view)
        activityIndicator.hidesWhenStopped = true
        activityIndicator .startAnimating()
    }
    
    /// This method hides activity indicator on view
    func hideActivityIndicator()
    {
        //Activity Indicator
        for view in self.subviews
        {
            if view.tag == 99998888
            {
                let activityIndicatorView = view.subviews[0] as! UIActivityIndicatorView
                activityIndicatorView .stopAnimating()
                activityIndicatorView .removeFromSuperview()
                view.removeFromSuperview()
            }
        }
    }
}
