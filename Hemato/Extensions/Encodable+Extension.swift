//
//  Encodable+Extension.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

extension Encodable {
    
    func getJsonData() -> Data? {
        do {
            let jsonEncoder = JSONEncoder()
            if #available(iOS 13.0, *) {
                jsonEncoder.outputFormatting = .withoutEscapingSlashes
            }
            let jsonData = try jsonEncoder.encode(self)
            return jsonData
        } catch {
            print(error)
        }
        return nil
    }
    
    func getJsonString() -> String? {
        var jsonString: String?
        if let data = getJsonData() {
            jsonString = String(decoding: data, as: UTF8.self)
            jsonString = jsonString?.replacingOccurrences(of: "\\/", with: "/")
        }
        return jsonString
    }
    
    func createJSONValue() -> [String: Any]? {
        if let jsonData = self.getJsonData() {
            let json = (try? JSONSerialization.jsonObject(with: jsonData,
                                                          options: .fragmentsAllowed) as?  [String: Any]) ?? [:]
            return json
        }
        return nil
    }
}
