//
//  ForgotPasswordViewController.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func forgotPasswordButtonClicked(_ sender: UIButton) {
    }
    
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
