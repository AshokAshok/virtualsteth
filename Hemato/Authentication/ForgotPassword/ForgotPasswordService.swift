//
//  ForgotPasswordService.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

protocol ForgotPasswordService {
    func forgotPassword(userName: String, onCompletion: @escaping (Result<Bool, Error>) -> Void)
}

struct ForgotPasswordServiceImpl: ForgotPasswordService  {
    
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func forgotPassword(userName: String,
                        onCompletion: @escaping (Result<Bool, Error>) -> Void) {
        let networkRequest = createNetworkRequest()
        networkClient.sendRequest(request: networkRequest) { (data, urlResponse, error) in
            
        }
    }
    
    func createNetworkRequest() -> NetworkRequest {
        let endpointURL = baseURL + ""
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .put,
                                            data: nil)
        return networkRequest
    }
}
