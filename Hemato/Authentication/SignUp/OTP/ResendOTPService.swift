//
//  ResendOTPService.swift
//  Hemato
//
//  Created by RamaKrishna on 19/05/21.
//

import Foundation

struct ResendOTPRequest {
    let otpReference: String
    let otp: String
}

struct ResendOTPResponse: Codable {
    let otp: String
    let otpReference: String
    let success: Bool
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case otp = "otp"
        case otpReference = "reference"
        case success = "success"
        case message = "message"
    }
}
protocol ResendOTPService {
    func resendOTP(request: ResendOTPRequest,
                        onCompletion: @escaping (Result<ResendOTPResponse, Error>) -> Void)
}

struct ResendOTPServiceImpl: ResendOTPService  {
    
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func resendOTP(request: ResendOTPRequest,
                   onCompletion: @escaping (Result<ResendOTPResponse, Error>) -> Void) {
        let networkRequest = createNetworkRequest(request: request)
        networkClient.sendRequest(request: networkRequest) { (data, urlResponse, error) in
            if let data = data,
               let response =  try? JSONDecoder().decode(ResendOTPResponse.self, from: data) {
                onCompletion(.success(response))
            } else {
                onCompletion(.failure(NetworkError.unknown()))
            }
        }
    }
    
    func createNetworkRequest(request: ResendOTPRequest) -> NetworkRequest {
        let subURL = "verification?otp_ref=\(request.otpReference)&otp=\(request.otp)"
        let endpointURL = baseURL + subURL
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .post,
                                            data: nil)
        return networkRequest
    }
}
