//
//  OTPViewModel.swift
//  Hemato
//
//  Created by RamaKrishna on 19/05/21.
//

import Foundation

protocol OTPViewDelegate {
    func onResendOTPSuccess()
    func onResendOTPFailure()
}

class OTPViewModel {
    
    var response: SignUpResponse
    let service: ResendOTPService
    
    var delegate: OTPViewDelegate?
    
    init(response: SignUpResponse,
         resendOTPService: ResendOTPService = ResendOTPServiceImpl()) {
        self.response = response
        self.service = resendOTPService
    }
    
    func isValidOTP(otp: String) -> Bool {
        if otp == response.otp {
            return true
        }
        return false
    }
    
    func resendOTP() {
        let request = ResendOTPRequest(otpReference: response.otpReference ?? "",
                                       otp: response.otp ?? "")
        
        service.resendOTP(request: request) { (result: Result<ResendOTPResponse, Error>) in
            switch result {
            case .success(let response):
                self.updateResponse(resendOTPResponse: response)
                self.delegate?.onResendOTPSuccess()
            case .failure(_):
                self.delegate?.onResendOTPFailure()
            }
        }
    }
    
    func updateResponse(resendOTPResponse: ResendOTPResponse) {
        response.otp = resendOTPResponse.otp
        response.otpReference = resendOTPResponse.otpReference
    }
}
