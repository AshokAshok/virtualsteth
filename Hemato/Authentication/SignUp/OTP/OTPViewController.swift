//
//  OTPViewController.swift
//  Hemato
//
//  Created by ashok quad on 15/05/21.
//

import UIKit

class OTPViewController: UIViewController {
    @IBOutlet weak var otpFiledView: OTPFieldView!
    var enteredOTP: String = ""
    var viewModel: OTPViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupOtpView()
        //viewModel.delegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setupOtpView(){
            otpFiledView.fieldsCount = 4
            otpFiledView.fieldBorderWidth = 1
            otpFiledView.defaultBorderColor = UIColor.black
            otpFiledView.filledBorderColor = UIColor.green
        otpFiledView.cursorColor = UIColor(named: "PrimaryColor")!
        otpFiledView.displayType = .roundedCorner
            otpFiledView.fieldSize = 40
            otpFiledView.separatorSpace = 8
            otpFiledView.shouldAllowIntermediateEditing = false
            otpFiledView.delegate = self
            otpFiledView.initializeUI()
        }
    
    @IBAction func continueButtonClicked(_ sender: UIButton) {
//        if viewModel.isValidOTP(otp: enteredOTP) {
//            UIAlertController.show(title: "Sign up success ", message: "Please Use the credentials to login to the application", on: self) {
//                self.navigationController?.popToRootViewController(animated: true)
//            }
//        }
//        else {
            UIAlertController.show(title: "Invalid OTP", message: "Please Enter valid OTP", on: self)
        //}
    }
    
    @IBAction func resendButtonClicked(_ sender: UIButton) {
        self.view.showActivityIndicator()
        viewModel.resendOTP()
    }
    
}

extension OTPViewController: OTPFieldViewDelegate
{
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
            print("Has entered all OTP? \(hasEntered)")
            return false
        }
        
        func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
            return true
        }
        
        func enteredOTP(otp otpString: String) {
            print("OTPString: \(otpString)")
            enteredOTP = otpString
        }
}

extension OTPViewController: OTPViewDelegate {
    func onResendOTPSuccess() {
        self.view.hideActivityIndicator()
        UIAlertController.show(title: "Success", message: "OTP sent successfully", on: self)
    }
    
    func onResendOTPFailure() {
        self.view.hideActivityIndicator()
        UIAlertController.show(title: "Unable to Send OTP", message: "Not able to send OTP", on: self)
    }
    
    
}
