//
//  SignUpViewModel.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

protocol SignUpViewDelegate {
    func onSignUpSuccess(response: SignUpResponse)
    func onSignUpFailure(message: String)
}

class SignUpViewModel {
    
    let signUpService: SignUpService
    
    var delegate: SignUpViewDelegate?
    
    init(signUpService: SignUpService = SignUpServiceImpl()) {
        self.signUpService = signUpService
    }
    
    func signUp(request: SignUpRequest) {
        signUpService.signUp(request: request) { (result: Result<SignUpResponse, Error>) in
            switch result {
            case .success(let response):
                self.delegate?.onSignUpSuccess(response: response)
            case .failure(let error):
                let errorMsg = self.createErrorMsg(error: error)
                self.delegate?.onSignUpFailure(message: errorMsg)
            }
        }
    }
    
    func createErrorMsg(error: Error) -> String {
        var errorMsg: String
        switch error {
        case is NetworkError:
            errorMsg = (error as! NetworkError).errorMsg()
        default:
            errorMsg = "Not Able to sign up"
            
        }
        return errorMsg
    }
    
    
}
