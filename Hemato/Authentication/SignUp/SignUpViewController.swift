//
//  SignUpViewController.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var viewModel: SignUpViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel = SignUpViewModel()
        viewModel.delegate = self
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func registerButtonClicked(_ sender: UIButton) {
        if let request = createSignUpRequest() {
            self.view.showActivityIndicator()
            viewModel.signUp(request: request)
        } else {
            UIAlertController.show(title: "Mandatory", message: "Please enter all the fields", on: self)
        }
    }
    
    func createSignUpRequest() -> SignUpRequest? {
        guard let userName = usernameField.text, !userName.isEmpty,
              let email = emailField.text, !email.isEmpty,
              let mobile = phoneField.text, !mobile.isEmpty,
              let password = passwordField.text, !password.isEmpty else { return nil }
        let request = SignUpRequest(name: userName,
                                    email: email,
                                    mobile: mobile,
                                    password: password)
        return request
        
    }
    
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func navigateToOTPVC(response: SignUpResponse) {
        let storyBoard: UIStoryboard=UIStoryboard.init(name: "Authentication", bundle: nil)
        
        let viewController: OTPViewController=storyBoard.instantiateViewController(identifier: "OTPViewController") as! OTPViewController
        viewController.viewModel = OTPViewModel(response: response)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SignUpViewController: SignUpViewDelegate {
    func onSignUpSuccess(response: SignUpResponse) {
        self.view.hideActivityIndicator()
        navigateToOTPVC(response: response)
    }
    
    func onSignUpFailure(message: String) {
        self.view.hideActivityIndicator()
//        let response = SignUpResponse(otp: "1234", otpReference: "1234", success: true, message: "")
//        navigateToOTPVC(response: response)
        UIAlertController.show(title: "Unable to Sign up", message:message , on: self)
    }
    
    
}
