//
//  SignUpService.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

struct SignUpRequest: Codable {
    let name: String
    let email: String
    let mobile: String
    let password: String
    
    enum CodingKeys: String, CodingKey {
        case name = "fullname"
        case email = "email"
        case mobile = "mobile"
        case password = "password"
    }
}

struct SignUpResponse: Codable {
    var otp: String?
    var otpReference: String?
    let success: Int
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case otp = "otp"
        case otpReference = "reference"
        case success = "success"
        case message = "message"
        
    }
}

protocol SignUpService {
    func signUp(request: SignUpRequest,
                        onCompletion: @escaping (Result<SignUpResponse, Error>) -> Void)
}

struct SignUpServiceImpl: SignUpService  {
    
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func signUp(request: SignUpRequest,
                onCompletion: @escaping (Result<SignUpResponse, Error>) -> Void) {
        let networkRequest = createNetworkRequest(request: request)
        networkClient.sendRequest(request: networkRequest) { (data, urlResponse, error) in
            if let data = data {
                do {
                    let resposne = try JSONDecoder().decode(SignUpResponse.self, from: data)
                    if (resposne.success != 0) {
                        onCompletion(.success(resposne))
                    } else {
                        onCompletion(.failure(NetworkError.unknown(message: resposne.message)))
                    }
                    
                } catch  {
                    print(error)
                    onCompletion(.failure(NetworkError.unknown()))
                }
                
            } else {
                onCompletion(.failure(NetworkError.unknown()))
            }
        }
    }
    
    func createNetworkRequest(request: SignUpRequest) -> NetworkRequest {
        let subURL = "register?fullname=\(request.name.encodededString())&email=\(request.email)&mobile=\(request.mobile)&password=\(request.password)&gcm_regid=1"
        let endpointURL = baseURL + subURL
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .get,
                                            data: nil)
        return networkRequest
    }
}
