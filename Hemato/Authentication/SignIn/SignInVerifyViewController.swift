//
//  SignInVerifyViewController.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import UIKit

class SignInVerifyViewController: UIViewController, UITextFieldDelegate {
    
    var signInViewModel: SignInViewModel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch HTUser.shared.currentRole {
        case .loggedIn:
            navigateToDashBoardVC()
        case.guest:
            self.signInViewModel = SignInViewModel()
            self.signInViewModel.delegate = self
        }
    }
    
    
    @IBAction func SignInBtnAction(_ sender: UIButton) {
        self.navigateToSigninVC()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text?.count ?? 0>0 {
            loginButton.setTitle("Login", for: .normal)
        }
        
        else
        {
            loginButton.setTitle("Login using OTP", for: .normal)
        }
        
        return true
    }
    
    @IBAction func skipButtonClicked(_ sender: UIButton?) {
        navigateToDashBoardVC()
    }
    
    func navigateToSigninVC() {
        let storyBoard: UIStoryboard=UIStoryboard.init(name: "Authentication", bundle: nil)
        
        let viewController: SignInViewController=storyBoard.instantiateViewController(identifier: "SignInViewController") as! SignInViewController
        viewController.email = emailField.text ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToDashBoardVC() {
        let storyBoard: UIStoryboard=UIStoryboard.init(name: "Product", bundle: nil)
        let navigationController: UINavigationController = storyBoard.instantiateViewController(identifier: "DashboardNavigationVC") as! UINavigationController
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftMenuVC: LeftMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "LeftMenuVC") as! LeftMenuVC
        
        let rootController = FAPanelController()
        rootController.configs.rightPanelWidth = 80
        rootController.center(navigationController).left(leftMenuVC)
        UIApplication.shared.windows.first?.rootViewController = rootController
    }
    
}

extension SignInVerifyViewController: SignInViewDelegate {
    func onLoginSuccess() {
        view.hideActivityIndicator()
        navigateToDashBoardVC()
    }
    
    func onLoginFailure(message: String) {
        view.hideActivityIndicator()
        UIAlertController.show(title: "Unable To Login", message: message, on: self)
    }
    
    
}
