//
//  SigninService.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

struct SignInRequest: Codable {
    let userName: String
    let password: String
    let accountTypes: [String]
    let deviceType: String
    let deviceToken: String
    let deviceId: String
    
    enum CodingKeys: String, CodingKey {
        case userName = "userName"
        case password = "password"
        case accountTypes = "accountTypes"
        case deviceType = "deviceType"
        case deviceToken = "deviceToken"
        case deviceId = "deviceId"
    }
}

struct User: Codable {
    let accountId: Int
    let referenceId: Int
    let encryptedReferenceId: String?
    let fullName: String?
    let roleId: Int
    let roleName: String?
    let email: String?
    let mobile: String?
    let countryId: Int
    let countryCode: String?
    let countryName: String?
    //let isAgreed: Int?
    //let isNew: Int?
    let isPasswordCreated: Bool
    let token: String?
    let referenceToken: String?
    let referralCode: String?
    
    enum CodingKeys: String, CodingKey {
        case accountId = "accountId"
        case referenceId = "referenceId"
        case encryptedReferenceId = "encryptedReferenceId"
        case fullName = "fullName"
        case roleId = "roleId"
        case roleName = "roleName"
        case email = "email"
        case mobile = "mobile"
        case countryId = "countryId"
        case countryCode = "countryCode"
        case countryName = "countryName"
//        case isAgreed = "isAgreed"
//        case isNew = "isNew"
        case isPasswordCreated = "isPasswordCreated"
        case token = "token"
        case referenceToken = "referenceToken"
        case referralCode = "referralCode"
    }
}

protocol SignInService {
    func signIn(request: SignInRequest, subURL: String,
                        onCompletion: @escaping (Result<User, Error>) -> Void)
}

struct SignInServiceImpl: SignInService  {
    
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func signIn(request: SignInRequest, subURL: String,
                onCompletion: @escaping (Result<User, Error>) -> Void) {
//        let networkRequest = createNetworkRequest(request: request, subURL: subURL)
//        networkClient.sendRequest(request: networkRequest) { (data, urlResponse, error) in
//            if let data = data {
//                do {
//                    let resposne = try JSONDecoder().decode(User.self, from: data)
//                    if (resposne.success != 0) {
//                        onCompletion(.success(resposne))
//                    } else {
//                        onCompletion(.failure(NetworkError.unknown(message: resposne.message)))
//                    }
//
//                } catch  {
//                    print(error)
//                    onCompletion(.failure(NetworkError.unknown()))
//                }
//
//            } else {
//                onCompletion(.failure(NetworkError.unknown()))
//            }
//        }
        
        let subURL = subURL
        let endpointURL = baseURL + subURL
        networkClient.sendAFRequest(requestUrl: endpointURL, parameters: request) { (data, error) in
            if let data = data {
                do {
                    let resposne = try JSONDecoder().decode(User.self, from: data)
                    if (resposne.accountId != 0) {
                        onCompletion(.success(resposne))
                    } else {
                        onCompletion(.failure(NetworkError.unknown(message: "")))
                    }
                    
                } catch  {
                    print(error)
                    onCompletion(.failure(NetworkError.unknown()))
                }
                
            } else {
                onCompletion(.failure(NetworkError.unknown()))
            }
        }
    }
    
    func createNetworkRequest(request: SignInRequest, subURL: String) -> NetworkRequest {
        let subURL = subURL
        let endpointURL = baseURL + subURL
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .post,
                                            data: nil)
        return networkRequest
    }
}
