//
//  SignInViewModel.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

protocol SignInViewDelegate {
    func onLoginSuccess()
    func onLoginFailure(message: String)
}

class SignInViewModel {
    
    let signInService: SignInService
    var delegate: SignInViewDelegate?
    
    init(signInService: SignInService = SignInServiceImpl()) {
        self.signInService = signInService
    }
    
    func signIn(request: SignInRequest, subURL: String) {
        signInService.signIn(request: request, subURL: subURL) { (result: Result<User, Error>) in
            switch result {
            case .success(let response):
                HTUserDefaults.shared.save(user: response)
                self.delegate?.onLoginSuccess()
            case .failure(let error):
                let errorMessage = self.createErrorMsg(error: error)
                self.delegate?.onLoginFailure(message: errorMessage)
            }
        }
    }
    
    func createErrorMsg(error: Error) -> String {
        var errorMsg: String
        switch error {
        case is NetworkError:
            errorMsg = (error as! NetworkError).errorMsg()
        default:
            errorMsg = "Please check the credentials"
            
        }
        return errorMsg
    }
}

