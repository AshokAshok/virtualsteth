//
//  ChangePasswordService.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

struct ChangePasswordRequest: Codable {
    let oldPassword: String
    let currentPassword: String
    
    enum CodingKeys: String, CodingKey {
        case oldPassword = "oldPassword"
        case currentPassword = "currentPassword"
    }
}

protocol ChangePasswordService {
    func changePassword(request: ChangePasswordRequest,
                        onCompletion: @escaping (Result<Bool, Error>) -> Void)
}

struct ChangePasswordServiceImpl: ChangePasswordService  {
    
    let networkClient: NetworkClient
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func changePassword(request: ChangePasswordRequest,
                        onCompletion: @escaping (Result<Bool, Error>) -> Void) {
        let networkRequest = createNetworkRequest(request: request)
        networkClient.sendRequest(request: networkRequest) { (data, urlResponse, error) in
            
        }
    }
    
    func createNetworkRequest(request: ChangePasswordRequest) -> NetworkRequest {
        let endpointURL = baseURL + ""
        let data = request.getJsonData()
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .put,
                                            data: data)
        return networkRequest
    }
}
