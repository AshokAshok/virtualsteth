//
//  CouponsService.swift
//  Hemato
//
//  Created by RamaKrishna on 14/05/21.
//

import Foundation

protocol CouponsService {
    func getCoupons(onCompletion: @escaping (Result<[Coupon], Error>) -> Void)
}

struct CouponsServiceImpl: CouponsService {
    
    let networkClient: NetworkClient
    var coupons = [Coupon]()
    
    init(networkClient: NetworkClient = NetworkClientImpl()) {
        self.networkClient = networkClient
    }
    
    func getCoupons(onCompletion: @escaping (Result<[Coupon], Error>) -> Void) {
        let request = createNetworkRequest()
        networkClient.sendRequest(request: request) { (data, response, error) in
            
        }
    }
    
    
    func createNetworkRequest() -> NetworkRequest {
        let endpointURL = baseURL + ""
        let networkRequest = NetworkRequest(endpointURL: endpointURL,
                                            httpMethod: .get,
                                            data: nil)
        return networkRequest
    }
    
}
