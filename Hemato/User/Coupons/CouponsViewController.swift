//
//  CouponsViewController.swift
//  Hemato
//
//  Created by RamaKrishna on 14/05/21.
//

import UIKit

class CouponsViewController: UIViewController {

    var couponsViewModel: CouponsViewModel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.couponsViewModel = CouponsViewModel()
        self.couponsViewModel.delegate = self
        // Do any additional setup after loading the view.
    }
    
}

extension CouponsViewController: CouponsViewDelegate {
    
    func showFailureAlert(title: String, message: String) {
        
    }
    
    
    func updateUI() {
        
        
    }
}
