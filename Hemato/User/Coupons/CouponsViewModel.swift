//
//  CouponsViewModel.swift
//  Hemato
//
//  Created by RamaKrishna on 14/05/21.
//

import Foundation

protocol CouponsViewDelegate {
    func updateUI()
    func showFailureAlert(title: String, message: String)
}

class CouponsViewModel {
    
    let couponsService: CouponsService
    
    var delegate: CouponsViewDelegate?
    
    private var coupons = [Coupon]()
    
    init(couponsService: CouponsService = CouponsServiceImpl()) {
        self.couponsService = couponsService
    }
    
    func getCoupons() {
        couponsService.getCoupons { (result: Result<[Coupon], Error>) in
            switch result {
            
            case .success(let coupons):
                self.coupons = coupons
                self.delegate?.updateUI()
            case .failure(_):
                self.delegate?.showFailureAlert(title: "Title", message: "Message")
            }
        }
    }
    
    func getNumberOfRows() -> Int {
        return coupons.count
    }
    
    func getItem(at index: Int) -> Coupon {
        return coupons[index]
    }
}
