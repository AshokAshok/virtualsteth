//
//  NetworkClient.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

import Alamofire

// MARK: - NetworkError

/// _NetworkError_ is an enumeration that specifies network errors
///
/// - generic:    Generic error
/// - invalidURL: Invalid URL error
enum NetworkError: Error {
    case invalidURL
    case noInternetConnection
    case serverError
    case unknown(message: String? = nil)
    
    
    func errorMsg() -> String {
        switch self {
        case .noInternetConnection:
            return "Please check your internet connection"
        case .serverError:
            return "Server is under maintainance. Please try again later"
        case .unknown(message: let message):
            return message ?? "Please try again"
        default:
            return "Please try again"
        }
    }
}


// MARK: - NetworkClientProtocol

/// _NetworkClientProtocol_ is a protocol specifies send network requests behaviour
protocol NetworkClient {

    func sendRequest(request: NetworkRequest, completion: @escaping (Data?, URLResponse?, Error?) -> ())
    func sendAFRequest(requestUrl: String, parameters:SignInRequest, completion: @escaping (Data?, Error?) -> ())
}


// MARK: - NetworkClient

/// _NetworkClient_ is a class responsible for network requests
struct NetworkClientImpl: NetworkClient {

    static let sharedInstance = NetworkClientImpl()

    let session: URLSession!


    // MARK: - Initialisers

    /// Initializes an instance of _NetworkClient_
    ///
    /// - returns: The instance of _NetworkClient_
    init() {

        let configuration = URLSessionConfiguration.default
        configuration.urlCache = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        configuration.timeoutIntervalForRequest = 60.0
        session = URLSession(configuration: configuration)
    }


    // MARK: - Send requests

    /// Sends a URL request
    ///
    /// - parameter request:    The URL request
    /// - parameter completion: The completion block
    func sendRequest(request: NetworkRequest, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        
        guard let urlRequest = request.createURLRequest() else { return completion(nil, nil, NetworkError.invalidURL) }
        print("URL = \(String(describing: urlRequest.url))")
        session.dataTask(with: urlRequest) {  data, response, error in
            DispatchQueue.main.async {
                let networkError = self.createNetworkErrorIfRequired(error: error)
                printResponse(data: data, response: response, error: error)
                completion(data, response, networkError)
            }
            
        }.resume()
    }
    
    func sendAFRequest(requestUrl: String, parameters:SignInRequest, completion: @escaping (Data?, Error?) -> ()) {
        
//        guard let urlRequest = request.createURLRequest() else { return completion(nil, nil, NetworkError.invalidURL) }
//        print("URL = \(String(describing: urlRequest.url))")
//        session.dataTask(with: urlRequest) {  data, response, error in
//            DispatchQueue.main.async {
//                let networkError = self.createNetworkErrorIfRequired(error: error)
//                printResponse(data: data, response: response, error: error)
//                completion(data, response, networkError)
//            }
//            
//        }.resume()
        
        AF.request(requestUrl,
                   method: .post,
                   parameters: parameters,
                   encoder: JSONParameterEncoder.default).responseData { response in
                    //switch response.result {
                    //case .success:
                    let networkError = self.createNetworkErrorIfRequired(error: response.error)
                        completion(response.data, networkError)
                    //case let .failure(error):
                        //print(error)
                    //}
                }
    }
    
    func printResponse(data: Data?, response: URLResponse?, error: Error?) {
        if let data = data {
            let response = try? JSONSerialization.jsonObject(with: data, options: [])
            print("SuccessResponse = \(String(describing: response))")
        }
        else {
            print("failure Response = \(String(describing: response)) \n\n\n\n\n error = \(String(describing: error))")
        }

    }
    func createNetworkErrorIfRequired(error: Error?) -> Error? {
        guard let error = error else { return nil }
        let errorToReturn: Error
        switch (error as NSError).code {
        case -1002:
            errorToReturn = NetworkError.invalidURL
        case -1009:
            errorToReturn = NetworkError.noInternetConnection
        default:
            errorToReturn = error
        }
        return errorToReturn
    }
}
