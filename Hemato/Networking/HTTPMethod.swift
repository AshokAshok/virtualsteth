//
//  HTTPMethod.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 13/05/21.
//

import Foundation

public enum HTTPMethod: String {
    
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case patch = "PATCH"
    case delete = "DELETE"
}
