//
//  NetworkRequest.swift
//  Hemato
//
//  Created by Rama Krishna Talupula on 14/05/21.
//

import Foundation

struct NetworkRequest {
    let endpointURL: String
    var data: Data?
    let httpMethod: HTTPMethod
    
    init(endpointURL: String,
         httpMethod: HTTPMethod,
         data: Data?) {
        self.endpointURL = endpointURL
        self.httpMethod = httpMethod
        self.data = data
    }
    
    func createURLRequest() -> URLRequest? {
        
        guard let url = URL(string: endpointURL) else {
            print("Failed to construct url from path \(String(describing: endpointURL))")
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        request.httpBody = data
        
        return request
    }
}
